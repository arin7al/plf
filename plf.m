%% Block1
f1 = @(t) exp(-t.^2).*cosh(t);
fF1 = @(m) (sqrt(pi)/2)*(exp((1-2*1i*m - m.^2)/4) + exp((1+2*1i*m - m.^2)/4));
f2 = @(t) t.^3./(t.^4 + 4);
fF2 = @(m) (m<0).*(2*pi*1i*cos(m).*(1./(2*exp(m)) +exp(m)/2)) + (m>0).*(-2*pi*1i*cos(m).*(1./(2*exp(m)) +exp(m)/2));
fF2 = @(m) -1i*pi.*sign(m).*exp(-abs(m)).*cos(abs(m));
f3 = @(t) 3./(t.^2 + log(3 + t.^4));
f4 = @(t) exp(-abs(t))./(1 + atan(t).^2);
f5 = @(t) t.*exp(-t.^2);
fF5 = @(l) - 1 ./ 2 .* (1i .* l) .* sqrt(pi) .* exp((- l .^ 2) ./ 4);

step = 2.5;
inpLimVec = [-100,100];
hFigure = figure();

plotFT3(hFigure, f1, fF1 , step, inpLimVec, []);
